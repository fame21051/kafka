package com.learn;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerWithCallBackDemo {
    public static void main(String[] args) {
        final Logger logger = LoggerFactory.getLogger(ProducerWithCallBackDemo.class);

        final String BOOTSTRAP_SERVER="127.0.0.1:9092";

        // Set Properties for Producer
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // Create the Producer
        Producer<String, String> producer = new KafkaProducer<String, String>(properties);

        //Create a Producer Record
        ProducerRecord<String, String> record = new ProducerRecord<String, String>("first_topic", "Hello from Producer Callback implementation");

        producer.send(record, new Callback() {
            public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                if(e==null) {
                    logger.info("Received New Metadata" + "\n"+
                            "Topic :" + recordMetadata.topic() + "\n" +
                            "Partition" + recordMetadata.partition() + "\n" +
                            "Offset" + recordMetadata.offset() + "\n" +
                            "Timestamp" + recordMetadata.timestamp());
                } else {
                    logger.info("Excaption occured while producing message");
                }

            }
        });

        producer.flush();

        producer.close();
    }
}
