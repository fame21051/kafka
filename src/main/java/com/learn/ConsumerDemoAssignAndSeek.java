package com.learn;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerDemoAssignAndSeek {
    public static void main(String[] args) {
        final Logger logger = LoggerFactory.getLogger(ConsumerDemo.class);

        final String BOOTSTRAP_SERVER = "127.0.0.1:9092";
        String topic = "first_topic";
        String group_id = "java-consumer-group";

        // Set Properties for Consumer
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, group_id);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // Create Kafka Consumer
        Consumer<String, String> consumer = new KafkaConsumer<>(properties);

        // Assign and Seek are mostly used to replay data or to fetch a specific data

        // create a new topic partition
        TopicPartition topicPartition = new TopicPartition(topic, 0);
        int offSetToReadFrom = 5;

        // assign
        consumer.assign(Arrays.asList(topicPartition));

        // seek
        consumer.seek(topicPartition, offSetToReadFrom);

        boolean keepOnReading = true;
        int numberOfMessagesToRead = 10;
        int numberOfMessagesReadSoFar = 0;

        //Create a Producer Record
        while(keepOnReading) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for(ConsumerRecord<String, String> record : records) {
                logger.info("Key " + record.key() + "," + "Value " + record.value());
                logger.info("partition " + record.partition() + "," + "key " + record.key() + "," + "Offset " + record.offset());
                numberOfMessagesReadSoFar ++;
                if(numberOfMessagesReadSoFar == numberOfMessagesToRead) {
                    keepOnReading = false;
                    break;
                }
            }
        }
        logger.info("Exiting the Application");
    }
}
